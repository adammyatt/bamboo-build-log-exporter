package com.pronetbeans.bamboo.buildlogexport;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.fileserver.SystemDirectory;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.user.User;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Adam Myatt
 */
public class ExportPlanBuildLogsAction extends BambooActionSupport {

    private String zipFileDownload;
    private String excludeSize;
    private String planKey;
    BambooUser currentUser;
    private String numExcluded;
    private String currentUserName;
    private BandanaManager bandanaManager;
    private ResultsSummaryManager resultsSummaryManager;
    private BuildLoggerManager buildLoggerManager;
    private static final Logger log = Logger.getLogger(ExportPlanBuildLogsAction.class);

    @Override
    public String doInput() throws Exception {

        if (isCurrentlyLoggedInUser()) {
            return doDefault();
        } else {
            if (getUser() == null) {
                // send to login screen instead
                return LOGIN;
            } else {
                addActionError("You do not have permission to edit the Delicious profile of this user");
                return ERROR;
            }
        }
    }

    @Override
    public String doDefault() throws Exception {
        if (isCurrentlyLoggedInUser() && getCurrentUser() != null) {

            String currentActionUrl = getNavigationUrl();
            String planKeyExtracted = currentActionUrl.substring(currentActionUrl.indexOf("browse/") + "browse/".length(), currentActionUrl.indexOf("/buildlogexporter.action"));

            planKey = planKeyExtracted;

            return INPUT;
        } else {
            if (getUser() == null) {
                // send to login screen instead
                return LOGIN;
            } else {
                addActionError("You do not have permission to view the Delicious profile of this user");
                return ERROR;
            }
        }

    }

    @Override
    public String execute() throws Exception {

        if (!isCurrentlyLoggedInUser()) {
            addActionError("You do not have permissions to perform this action.");
            return ERROR;
        }

        if (getCurrentUser() != null) {

            ZipOutputStream out = null;

            try {

                int maxFileSize = 0;
                boolean useMaxFileSize = false;
                final int onekb = 1024;
                int numExcludedCount = 0;

                if (excludeSize == null || "-1".equals(excludeSize)) {
                    useMaxFileSize = false;

                } else if ("0".equals(excludeSize)) {
                    // 0 = 10KB
                    maxFileSize = onekb * 10;
                    useMaxFileSize = true;
                } else if ("1".equals(excludeSize)) {
                    // 1 = 100KB
                    maxFileSize = onekb * 100;
                    useMaxFileSize = true;
                } else if ("2".equals(excludeSize)) {
                    // 2= 500 KB
                    maxFileSize = onekb * 500;
                    useMaxFileSize = true;
                } else if ("3".equals(excludeSize)) {
                    // 3 = 1 MB
                    maxFileSize = onekb * 1000;
                    useMaxFileSize = true;
                } else if ("4".equals(excludeSize)) {
                    // 4 = 5 MB
                    maxFileSize = onekb * 5000;
                    useMaxFileSize = true;
                } else {
                    // though redundant, spell it out
                    useMaxFileSize = false;
                }

                String currentActionUrl = getNavigationUrl();
                String planKeyExtracted = currentActionUrl.substring(currentActionUrl.indexOf("browse/") + "browse/".length(), currentActionUrl.indexOf("/doBuildLogExporter.action"));

                Plan plan = getPlanManager().getPlanByKey(planKeyExtracted);

                List<String> jobKeys = new ArrayList<String>();
                Chain myPlan = planManager.getPlanByKey(plan.getPlanKey(), Chain.class);

                for (ChainStage chainStage : myPlan.getStages()) {

                    for (Job job : chainStage.getJobs()) {
                        String jobKey = job.getKey();
                        jobKeys.add(jobKey);
                    }
                }

                if (jobKeys != null && jobKeys.isEmpty()) {
                    addActionError("No job or stages, thus no build logs to export.");
                    return ERROR;
                }

                String firstJobKeyForUrl = jobKeys.get(0);

                zipFileDownload = "/download/" + firstJobKeyForUrl + "/" + planKeyExtracted + "-EXPORTED-LOGS.zip";

                File fileZipDirectory = SystemDirectory.getBuildLogsDirectory(firstJobKeyForUrl).getParentFile();

                out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(new File(fileZipDirectory,
                        planKeyExtracted + "-EXPORTED-LOGS.zip"))));

                Iterator<String> iter = jobKeys.iterator();
                while (iter.hasNext()) {

                    String jobKey = iter.next();

                    File filPlanStageJobKey = SystemDirectory.getBuildLogsDirectory(jobKey);

                    File[] allFilesinDir = filPlanStageJobKey.listFiles();

                    for (int k = 0; k < allFilesinDir.length; k++) {

                        FileInputStream in = null;
                        File file = allFilesinDir[k];

                        if (!useMaxFileSize || (useMaxFileSize && !(file.length() > maxFileSize))) {

                            try {
                                in = new FileInputStream(file);

                                // Add ZIP entry to output stream. 
                                out.putNextEntry(new ZipEntry(file.getName()));

                                // Transfer bytes from the file to the ZIP file 
                                byte[] buf = new byte[1024];

                                int len;
                                while ((len = in.read(buf)) > 0) {
                                    out.write(buf, 0, len);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    out.closeEntry();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    in.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            numExcludedCount++;
                        }
                    }
                }

                if (numExcludedCount > 0) {
                    this.numExcluded = "Excluded " + numExcludedCount + " log files due to file size limits.";
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            log.info(Utils.getLogBanner());

            return SUCCESS;

        } else {
            addActionError(getText("user.admin.edit.failed", new String[]{currentUserName}));
            return ERROR;
        }
    }

    public boolean isCurrentlyLoggedInUser() {
        if (getUser() == null || getCurrentUser() == null || getCurrentUser().getUser() == null) {
            return false;
        }

        return getCurrentUser().getUser().equals(getUser());
    }

    public BambooUser getCurrentUser() {
        if (currentUser == null) {
            if (StringUtils.isBlank(currentUserName)) {
                User user = getUser();
                if (user != null) {
                    currentUser = getBambooUserManager().getBambooUser(user.getName());
                }
            } else {
                currentUser = getBambooUserManager().getBambooUser(currentUserName);
            }
        }

        return currentUser;
    }

    /**
     * @return the bandanaManager
     */
    public BandanaManager getBandanaManager() {
        return bandanaManager;
    }

    /**
     * @param bandanaManager the bandanaManager to set
     */
    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    /**
     * @return the resultsSummaryManager
     */
    public ResultsSummaryManager getResultsSummaryManager() {
        return resultsSummaryManager;
    }

    /**
     * @param resultsSummaryManager the resultsSummaryManager to set
     */
    public void setResultsSummaryManager(ResultsSummaryManager resultsSummaryManager) {
        this.resultsSummaryManager = resultsSummaryManager;
    }

    /**
     * @return the buildLoggerManager
     */
    public BuildLoggerManager getBuildLoggerManager() {
        return buildLoggerManager;
    }

    /**
     * @param buildLoggerManager the buildLoggerManager to set
     */
    public void setBuildLoggerManager(BuildLoggerManager buildLoggerManager) {
        this.buildLoggerManager = buildLoggerManager;
    }

    /**
     * @return the zipFileDownload
     */
    public String getZipFileDownload() {
        return zipFileDownload;
    }

    /**
     * @param zipFileDownload the zipFileDownload to set
     */
    public void setZipFileDownload(String zipFileDownload) {
        this.zipFileDownload = zipFileDownload;
    }

    /**
     * @return the excludeSize
     */
    public String getExcludeSize() {
        return excludeSize;
    }

    /**
     * @param excludeSize the excludeSize to set
     */
    public void setExcludeSize(String excludeSize) {
        this.excludeSize = excludeSize;
    }

    /**
     * @return the numExcluded
     */
    public String getNumExcluded() {
        return numExcluded;
    }

    /**
     * @param numExcluded the numExcluded to set
     */
    public void setNumExcluded(String numExcluded) {
        this.numExcluded = numExcluded;
    }

    /**
     * @return the planKey
     */
    public String getPlanKey() {
        return planKey;
    }

    /**
     * @param planKey the planKey to set
     */
    public void setPlanKey(String planKey) {
        this.planKey = planKey;
    }
}
