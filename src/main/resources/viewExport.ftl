<html>
<head>
    [@ui.header pageKey="Bamboo Build Log Exporter" title=true /]
     <meta name="tab" content="plan">  
</head>
<body>
<br>
<hr>

[@ww.form id='exportPlanBuildLogsPlugin' action='doBuildLogExporter.action' titleKey='Export Plan Build Logs']
<br><br>
  Exclude Logs Over This Size : <select name='excludeSize' id='excludeSize' size='1'>
      <option value="-1">No Limit</option>
      <option value="0">10 KB</option>      
      <option value="1">100 KB</option>
      <option value="2">500 KB</option>
      <option value="3">1 MB</option>
      <option value="4">5 MB</option>
   </select>
<br><br>
    [#if zipFileDownload?has_content]
<br><br><br>     
     [@ui.clear /]
      <a href="${zipFileDownload}">Download Exported Build Logs</a>
      
      <br>

        [@ui.clear /]
    [/#if]
    [#if numExcluded?has_content]
        <br><br>
        <b>${numExcluded}</b>
    [/#if]

<div class="buttons-container">
  <div class="buttons">
     <input class="button submit" type="submit" id="save-btn1" value="Export"/>
     <a class="cancel" href="/browse/${planKey}">Cancel</a>
  </div>
</div>

[/@ww.form]
<br>
<br>

</body>
</html>